FROM mhart/alpine-node:14 AS builder
LABEL stage=builder
ARG ENV_NAME
WORKDIR /app
COPY . .
ENV REACT_APP_ENV=$ENV_NAME
RUN yarn
RUN yarn run build

FROM mhart/alpine-node
RUN yarn global add serve
WORKDIR /app
COPY --from=builder /app/build .
CMD ["serve", "-p", "8421", "-s", "."]
